#!/usr/bin/env python

from setuptools import find_packages, setup

setup_requires = ['pytest-runner>=2.6.2', 'setuptools>=17.1']

tests_requires = [
    'bandit>=1.6.2',
    'check-manifest>=0.25',
    'coverage>=4.0',
    'flake8>=3.8.1',
    'isort>=5.0.0',
    'mock>=2.0.0',
    'mypy>=0.780',
    'pydocstyle>=1.0.0',
    'pytest-cache>=1.0',
    'pytest-cov>=1.8.0',
    'pytest-pep8>=1.0.6',
    'pytest>=3.8.0',
    'safety>=1.9.0',
]

install_requires = [
    'arrow>=0.15.0',
    'dojson>=1.4.0',
    'jsonschema>=2.5.1',
    'loguru>=0.5.0',
    'more-itertools>=8.8.0',
]
extras_require = {
    'docs': ['Sphinx>=3.0.4'],
    'tests': tests_requires,
    'dev': ['ipdb', 'jupyter', 'ipython'],
}

packages = find_packages()

extras_require['all'] = []
for name, reqs in extras_require.items():
    extras_require['all'].extend(reqs)

setup(
    extras_require=extras_require,
    include_package_data=True,
    install_requires=install_requires,
    name='translations',
    packages=packages,
    python_requires='>=3.6',
    setup_requires=setup_requires,
    test_suite='tests',
    tests_require=tests_requires,
    zip_safe=False,
)
