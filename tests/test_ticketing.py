# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Test Accounts."""

import json
import os

import pytest
from translations import ticketing
from jsonschema import ValidationError, validate
from .helpers import ordered


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ({}, {}),  # External Ids
    ],
)
def test_fields(test_input, expected):
    """General simple field test.

    The first element of the list is the dictionary we receive from the source,
    i.e. ``{'Full_Account_ID__c': '0011U00000Nfvg9QAB'}`` and the second is the
    output from the translation i.e.
    ``{'external_ids': [{'origin': 'sf', 'id': '0011U00000Nfvg9QAB'}]}``.
    """
    # Add default fields to the expected value
    res = ticketing.translate(test_input)
    if res:
        # Remove field added by post-process
        # assert res.pop('scores', None) is not None
        assert (
            res.pop('organization_id')
            == 'test-uuid'
        )
    assert ordered(res) == ordered(expected)


def test_account_valid_info():
    """Test schema validation."""
    pwd = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(pwd, 'ticketing.json')) as f:
        schema = json.load(f)

    with pytest.raises(ValidationError):
        validate({}, schema)

    raw = {
        'Account__c': '0010c000024PQ8vAAG',
        'Amount_Paid__c': 0,
        'Balance__c': 0,
        'Basis__c': 'M',
        'CreatedById': '0051I000000Od5oQAC',
        'CreatedDate': '2018-12-02T14:22:50.000+0000',
        'Current_Fiscal_Months_Trans__c': 'Jun',
        'Disposition_Code__c': 'UPS2',
        'Full_Transaction_ID__c': 'a0B0c00000utnTvEAI',
        'Id': 'a0B0c00000utnTvEAI',
        'Item_Code__c': 'UCCS200',
        'Item_Ext__c': 0,
        'Item_Price__c': 0,
        'Item_Title__c': 'UConn at Madison Square Garden Courtside VIP - $200',
        'LastModifiedById': '0051I000000Od5oQAC',
        'LastModifiedDate': '2018-12-02T18:21:42.000+0000',
        'Name': 'MBB19: UCCS200',
        'Order_Date__c': '2018-12-02',
        'Order_Line_ID__c': 'MBB19:74711:1',
        'Order_Quantity__c': 2,
        'Orig_Salecode_Name__c': 'Internet',
        'Orig_Salecode__c': 'I',
        'Patron_ID__c': '74711',
        'Reporting_Date__c': '2019-01-01',
        'Season_Code__c': 'MBB19',
        'SystemModstamp': '2018-12-02T18:21:43.000+0000',
        'Transaction_Month__c': 'Dec',
        'Transaction_Report_Year__c': '2019',
    }

    account = ticketing.translate(raw)

    # print(account)

    validate(account, schema)
