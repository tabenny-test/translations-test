# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Test Accounts."""

import json
import os

import pytest
from translations import accounts
from jsonschema import ValidationError, validate
from .helpers import ordered


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ({}, {}),  # External Ids
    ],
)
def test_fields(test_input, expected):
    """General simple field test.

    The first element of the list is the dictionary we receive from the source,
    i.e. ``{'Full_Account_ID__c': '0011U00000Nfvg9QAB'}`` and the second is the
    output from the translation i.e.
    ``{'external_ids': [{'origin': 'sf', 'id': '0011U00000Nfvg9QAB'}]}``.
    """
    # print("Test Input:",test_input)
    # Add default fields to the expected value
    res = accounts.translate(test_input)
    print("11111111111111111111111")
    if res:
        # Remove field added by post-process
        assert res.pop('scores', None) is not None
        assert res.pop('organization_id') == 'test-uuid'
    else:
        print("No value so its going to be an error")

    # print(res)
    # print(expected)
    assert ordered(res) == ordered(expected)


def test_account_valid_info():
    """Test schema validation."""
    pwd = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(pwd, 'accounts.json')) as f:
        schema = json.load(f)

    with pytest.raises(ValidationError):
        validate({}, schema)

    raw = {
        'Id': '0010c000024PlI2AAK',
        'Patron_ID__c': '55851',
        'Banner_ID__c': '01941463',
        'Full_Account_ID__c': '0011I00000jhWCaQAM',
        'OwnerId': '0011I00000jhWCaQAM',
        'LastName': 'Last',
        'FirstName': 'First',
        'PersonBirthdate': '1990-01-01',
        'Phone': '(999)-999-8888',
        'PersonHomePhone': '999/999-8888',
        'PersonEmail': 'test@test.com',
        'BillingAddress': {'city': 'New York', 'postalCode': '10036'},
        'PersonOtherCountry': 'SP',
        'PersonOtherStreet': 'street',
        'PersonOtherPostalCode': '12345',
        'PersonMailingAddress': {'state': 'NC', 'postalCode': '27514'},
        'Men_s_Basketball_Full__c': True,
        'School_Alum__pc': True,
        'Customer_Type__c': 'YA',
        'School_Grad_Year__pc': '1990-01-10',
        'AnnualRevenue': '10000',
        'Nova_Points__c': 123,
        'Inactive__pc': None,
        'PersonDoNotCall': None,
        'Adobe_Opt_Out__pc': False,
        'Account_Flag__c': None,
    }

    account = accounts.translate(raw)

    # print("After translate **********************************")
    # print(account)
    validate(account, schema)
