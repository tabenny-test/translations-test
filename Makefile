clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -fr {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

test:	lint pytest  ## run all tests

lint: ## lint and static code analysis.
	flake8 translations tests
	bandit -r translations
	safety check --full-report
	isort . -rc --df --verbose

pytest: ## test suite
	pydocstyle translations tests
	python setup.py test

jupyter: ## start jupyter server serving on notebooks folder
	jupyter notebook

install: clean ## install the package to the active Python's site-packages
	pip install 'pip>21.1'
	pip install -e .[all]
