# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Accounts field translations."""

import arrow
from dojson import Overdo
from dojson.utils import for_each_value, ignore_value
from loguru import logger

from .factory import translation_factory
from .wrangling import basic_fix_string_value, clean_phone_number
from .wrangling import create_external_id, is_valid_email, postprocess_account

model = Overdo()
"""Account translations model."""


@model.over('external_ids', '^Full_Account_ID__c$')
@for_each_value
@ignore_value
def external_ids(self, key, value):
    """External ID."""
    if not value:
        logger.error('External ID {} not found', key)
        return None
    return create_external_id(key, value)


@model.over('creation_date', '^CreatedDate$')
@ignore_value
def creation_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Creation date not found')
        return None

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error('Cannot convert {} into date for creation date.', value)
        return None


@model.over('modification_date', '^LastModifiedDate$')
@ignore_value
def modification_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Modification date not found')
        return None

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error(
            'Cannot convert {} into date for modification date.', value
        )
        return None


@model.over('owner_id', '^OwnerId$')
@ignore_value
def owner_id(self, key, value):
    """Owner identifier, typically a sales representative."""
    if value is None:
        logger.debug('Owner ID not found')
        return
    return create_external_id(key, value)


@model.over('last_name', '^LastName$')
@ignore_value
def last_name(self, key, value):
    """Last name coming from Account."""
    return basic_fix_string_value(value) if value else None


@model.over('first_name', '^FirstName$')
@ignore_value
def first_name(self, key, value):
    """First name coming from Account."""
    return basic_fix_string_value(value) if value else None


@model.over('gender', '^__TODO__$')
@ignore_value
def gender(self, key, value):
    """Account gender."""
    # Check enum: F,M
    return value if value else None


@model.over('birthday', '^__TODO__$')
@ignore_value
def birthday(self, key, value):
    """Birthday coming from Account."""
    if not value:
        return None
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error('Cannot convert {} into date for birthday', value)
        return None


@model.over('email', '^PersonEmail$')
@for_each_value
@ignore_value
def email(self, key, value):
    """Email from account."""
    if not value or not is_valid_email(value):
        return None
    return value


@model.over('phone', '^Phone$')
@for_each_value
@ignore_value
def phone(self, key, value):
    """Known phone numbers from Account."""
    value = clean_phone_number(value)
    return value if value else None


@model.over('type', '^__TODO__$')
@ignore_value
def type(self, key, value):
    """Account type from Account."""
    TYPES = {'alumni': 'alumni', 'customer': 'fan', 'prospect': 'prospect'}

    if key == 'School_Alum__pc':
        if 'alumni' in self.get('type', []) and value is False:
            logger.warning(
                'Found {} account type but {} is False',
                self.get('type'), key
            )
        elif 'Alumni' not in self.get('type', []) and value is True:
            current_value = self.setdefault('type', [])
            self['type'].append('alumni')

    if key == 'Type' and value is not None:
        current_value = self.setdefault('type', [])
        value = TYPES.get(value.lower(), 'other')
        if value not in current_value:
            self['type'].append(value)
    return value if value else None


@model.over('address', '^BillingAddress$')
@for_each_value
@ignore_value
def address(self, key, value):
    """Known addresses."""
    return value if value else None


@model.over('booster_level', '^__TODO__$')
@ignore_value
def booster_level(self, key, value):
    """Account booster level."""
    return value if value else None


@model.over('booster_start_date', '^__TODO__$')
@ignore_value
def booster_start_date(self, key, value):
    """Account booster start date."""
    if not value:
        return None
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error(
            'Cannot convert {} into date for booster start date.', value
        )
        return None


@model.over('faculty_staff_start_date', '^__TODO__$')
@ignore_value
def faculty_staff_start_date(self, key, value):
    """Account faculty staff start date."""
    if not value:
        return None
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error(
            'Cannot convert {} into date for faculty staff start date', value
        )
        return None


@model.over('letterwinner', '^__TODO__$')
@for_each_value
@ignore_value
def letterwinner(self, key, value):
    """Letter winner from Account."""
    # TODO: set has_scholarship and alumni flags
    return value if value else None


@model.over('degree', '^__TODO__$')
@ignore_value
def degree(self, key, value):
    """Account degree."""
    return value if value else None


@model.over('salary', '^AnnualRevenue$')
@ignore_value
def salary(self, key, value):
    """Income from Account."""
    if not value:
        return None

    try:
        return int(value)
    except (ValueError, TypeError):
        logger.error('Cannot convert {} into integer for salary', value)
        return None


@model.over('company', '^__TODO__$')
@ignore_value
def company(self, key, value):
    """Company coming from Account.

    This field only applies if the account is a person. TODO: add check!
    """
    return value if value else None


@model.over('industry', '^Industry$')
@ignore_value
def industry(self, key, value):
    """Industry coming from Account."""
    return value if value else None


@model.over('number_of_employees', '^NumberOfEmployees$')
@ignore_value
def number_of_employees(self, key, value):
    """Cast number of employees coming from account.

    This should only apply to company accounts. TODO: add check!
    """
    if not value:
        return None

    self['is_company'] = True

    try:
        return int(value)
    except (ValueError, TypeError):
        logger.error(
            'Cannot convert {} into integer for number of employees.', value
        )
        return None


@model.over('social', '^__TODO__$')
@for_each_value
@ignore_value
def social(self, key, value):
    """Account social networks."""
    return value if value else None


@model.over('contact_preference', '^__TODO__$')
@for_each_value
@ignore_value
def contact_preference(self, key, value):
    """Account contact preference."""
    return value if value else None


# Flags


@model.over('is_active', '^Account_Flag__c$')
def is_active(self, key, value):
    """Is active."""
    return value in (True, 'Yes')


@model.over('do_not_call', '^PersonDoNotCall$')
def do_not_call(self, key, value):
    """Do not call."""
    return value in (True, 'Yes')


@model.over('do_not_mail', '^__TODO__$')
def do_not_mail(self, key, value):
    """Do not mail."""
    return value in (True, 'Yes')


@model.over('do_not_email', '^__TODO__$')
def do_not_email(self, key, value):
    """Do not email."""
    return value in (True, 'Yes')


translate = translation_factory(
    'test-uuid',
    model,
    id_='http://app.fanalytical.com/schemas/records/accounts-v1.0.0.json',
    postprocess=postprocess_account,
)
"""Transform account data from source to Fanalytical schema."""
